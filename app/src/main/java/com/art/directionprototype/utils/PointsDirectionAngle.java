package com.art.directionprototype.utils;

import android.location.Location;
import android.support.annotation.NonNull;

import com.art.directionlibrary.DirectionAngle;

/**
 * Created by Артём on 12.08.2015.
 * Угол между параллелью и направлением между двумя точками.
 */
public class PointsDirectionAngle extends DirectionAngle
{
    private float mDistance;

    private PointsDirectionAngle(double angle, float distance)
    {
        super(angle);
        mDistance = distance;
    }

    public static PointsDirectionAngle fromPoints(@NonNull Location startPoint, @NonNull Location finishPoint)
    {
        float[] result = new float[2];
        double startLatitude = startPoint.getLatitude();
        double startLongitude = startPoint.getLongitude();
        double finishLatitude = finishPoint.getLatitude();
        double finishLongitude = finishPoint.getLongitude();
        Location.distanceBetween(startLatitude, startLongitude, finishLatitude, finishLongitude, result);

        float angle = result[1];
        float distance = result[0];
        return new PointsDirectionAngle(angle >= 0 ? angle : 360 + angle, distance);
    }

    public float getDistance()
    {
        return mDistance;
    }

}
