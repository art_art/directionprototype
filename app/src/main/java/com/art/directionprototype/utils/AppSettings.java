package com.art.directionprototype.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import com.art.directionprototype.R;
import com.art.directionprototype.ui.LocationPreference;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by art090390 on 06.08.2015.
 * Application settings class.
 */
public final class AppSettings
{
    private Set<SettingsListener> mListeners;

    private String mCloseVolumeKey;
    private String mFarVolumeKey;
    private String mFinishPointKey;
    private String mUpdatePeriodKey;
    private String mSuccessBorderKey;
    private String mVolumeEditableKey;
    private String mSoundNotificationTypeKey;

    private int mCloseVolumeDefault;
    private int mFarVolumeDefault;
    private String mFinishPointDefault;
    private int mUpdatePeriodDefault;
    private int mSuccessBorderDefault;

    private SharedPreferences.OnSharedPreferenceChangeListener mPreferencesListener =
            new SharedPreferences.OnSharedPreferenceChangeListener()
            {
                private void notifyPreferenceChanged(String changedPreferenceKey, String preferenceKey, ListenersNotifier notifier)
                {
                    if (changedPreferenceKey.compareTo(preferenceKey) == 0)
                    {
                        notifyListeners(notifier);
                    }
                }

                @Override
                public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, String key)
                {
                    notifyPreferenceChanged(key, mFarVolumeKey, new ListenersNotifier()
                    {
                        @Override
                        public void notifyEvent(SettingsListener listener)
                        {
                            listener.onFarVolumeChanged(sharedPreferences, getFarVolume(sharedPreferences));
                        }
                    });

                    notifyPreferenceChanged(key, mCloseVolumeKey, new ListenersNotifier()
                    {
                        @Override
                        public void notifyEvent(SettingsListener listener)
                        {
                            listener.onCloseVolumeChanged(sharedPreferences, getCloseVolume(sharedPreferences));
                        }
                    });

                    notifyPreferenceChanged(key, mFinishPointKey, new ListenersNotifier()
                    {
                        @Override
                        public void notifyEvent(SettingsListener listener)
                        {
                            listener.onFinishPointChanged(sharedPreferences, getFinishPoint(sharedPreferences));
                        }
                    });

                    notifyPreferenceChanged(key, mUpdatePeriodKey, new ListenersNotifier()
                    {
                        @Override
                        public void notifyEvent(SettingsListener listener)
                        {
                            listener.onUpdatePeriodChanged(sharedPreferences, getUpdatePeriod(sharedPreferences));
                        }
                    });

                    notifyPreferenceChanged(key, mSuccessBorderKey, new ListenersNotifier()
                    {
                        @Override
                        public void notifyEvent(SettingsListener listener)
                        {
                            listener.onSuccessBorderChanged(sharedPreferences, getSuccessBorder(sharedPreferences));
                        }
                    });

                    notifyPreferenceChanged(key, mVolumeEditableKey, new ListenersNotifier()
                    {
                        @Override
                        public void notifyEvent(SettingsListener listener)
                        {
                            listener.onVolumeEditableChanged(sharedPreferences, getVolumeEditable(sharedPreferences));
                        }
                    });

                    notifyPreferenceChanged(key, mSoundNotificationTypeKey, new ListenersNotifier()
                    {
                        @Override
                        public void notifyEvent(SettingsListener listener)
                        {
                            listener.onSoundNotificationTypeChanged(sharedPreferences, getSoundNotificationType(sharedPreferences));
                        }
                    });
                }
            };

    public AppSettings(Context context)
    {
        mListeners = new HashSet<>();

        mCloseVolumeKey = context.getString(R.string.pref_key_close_volume);
        mCloseVolumeDefault = context.getResources().getInteger(R.integer.close_volume_default);

        mFarVolumeKey = context.getString(R.string.pref_key_far_volume);
        mFarVolumeDefault = context.getResources().getInteger(R.integer.far_volume_default);

        mFinishPointKey = context.getString(R.string.pref_key_finish_point);
        mFinishPointDefault = context.getString(R.string.location_default);

        mUpdatePeriodKey = context.getString(R.string.pref_key_update_period);
        mUpdatePeriodDefault = context.getResources().getInteger(R.integer.update_period_default);

        mSuccessBorderKey = context.getString(R.string.pref_key_success_border);
        mSuccessBorderDefault = context.getResources().getInteger(R.integer.success_border_default);

        mVolumeEditableKey = context.getString(R.string.pref_key_edit_volume);

        mSoundNotificationTypeKey = context.getString(R.string.pref_key_notification_type);
    }

    public void register(SharedPreferences sharedPreferences)
    {
        sharedPreferences.registerOnSharedPreferenceChangeListener(mPreferencesListener);
    }

    public void unregister(SharedPreferences sharedPreferences)
    {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(mPreferencesListener);
    }

    public synchronized void addListener(SettingsListener listener)
    {
        mListeners.add(listener);
    }

    public synchronized void removeListener(SettingsListener listener)
    {
        mListeners.remove(listener);
    }

    public String getFarVolumeKey()
    {
        return mFarVolumeKey;
    }

    public String getCloseVolumeKey()
    {
        return mCloseVolumeKey;
    }

    private String getFinishPointJson(SharedPreferences sharedPreferences)
    {
        return sharedPreferences.getString(mFinishPointKey, mFinishPointDefault);
    }

    public Location getFinishPoint(SharedPreferences sharedPreferences)
    {
        return LocationPreference.parseLocation(getFinishPointJson(sharedPreferences));
    }

    public int getCloseVolume(SharedPreferences sharedPreferences)
    {
        return sharedPreferences.getInt(mCloseVolumeKey, mCloseVolumeDefault);
    }

    public int getFarVolume(SharedPreferences sharedPreferences)
    {
        return sharedPreferences.getInt(mFarVolumeKey, mFarVolumeDefault);
    }

    public int getUpdatePeriod(SharedPreferences sharedPreferences)
    {
        return sharedPreferences.getInt(mUpdatePeriodKey, mUpdatePeriodDefault);
    }

    public int getSuccessBorder(SharedPreferences sharedPreferences)
    {
        return sharedPreferences.getInt(mSuccessBorderKey, mSuccessBorderDefault);
    }

    public boolean getVolumeEditable(SharedPreferences sharedPreferences)
    {
        return sharedPreferences.getBoolean(mVolumeEditableKey, true);
    }

    public int getSoundNotificationType(SharedPreferences sharedPreferences)
    {
        return Integer.parseInt(sharedPreferences.getString(mSoundNotificationTypeKey, "0"));
    }

    private synchronized void notifyListeners(ListenersNotifier notifier)
    {
        for (SettingsListener listener : mListeners)
        {
            notifier.notifyEvent(listener);
        }
    }

    private interface ListenersNotifier
    {
        void notifyEvent(SettingsListener listener);
    }

    public interface SettingsListener
    {
        void onCloseVolumeChanged(SharedPreferences sharedPreferences, int closeVolume);

        void onFarVolumeChanged(SharedPreferences sharedPreferences, int farVolume);

        void onFinishPointChanged(SharedPreferences sharedPreferences, Location finishPoint);

        void onUpdatePeriodChanged(SharedPreferences sharedPreferences, int updatePeriod);

        void onSuccessBorderChanged(SharedPreferences sharedPreferences, int successBorder);

        void onVolumeEditableChanged(SharedPreferences sharedPreferences, boolean editable);

        void onSoundNotificationTypeChanged(SharedPreferences sharedPreferences, int soundNotificationType);
    }
}
