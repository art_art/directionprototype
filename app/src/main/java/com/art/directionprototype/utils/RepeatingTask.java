package com.art.directionprototype.utils;

import android.os.Handler;

/**
 * Created by Артём on 13.08.2015.
 * Self repeating runnable.
 */
public abstract class RepeatingTask implements Runnable
{
    private Handler mHandler;
    private int mInterval;

    protected RepeatingTask(Handler handler, int interval)
    {
        mHandler = handler;
        mInterval = interval;
    }

    protected abstract void task();

    @Override
    public void run()
    {
        task();
        mHandler.postDelayed(this, mInterval);
    }

    public void setInterval(int interval)
    {
        mInterval = interval;
    }

    public void stopRepeatingTask()
    {
        mHandler.removeCallbacks(this);
    }

    private void start()
    {
        mHandler.post(this);
    }

    protected static void start(RepeatingTask task)
    {
        task.start();
    }
}
