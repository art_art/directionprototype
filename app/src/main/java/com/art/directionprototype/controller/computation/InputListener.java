package com.art.directionprototype.controller.computation;

import android.content.Context;
import android.location.Location;
import android.support.annotation.Nullable;

import com.art.directionlibrary.Director;
import com.google.android.gms.location.LocationListener;

/**
 * Created by Артём on 12.08.2015.
 * Input data for direction computing listener.
 */
public class InputListener implements LocationListener, SensorsValuesListener, FinishPointListener
{
    private Location mStartLocation;
    private Location mFinishLocation;
    private Director.SensorsValues mSensorsValues;

    public InputListener(Location finishPoint)
    {
        mFinishLocation = finishPoint;
    }

    @Override
    public void onLocationChanged(Location location)
    {
        mStartLocation = location;
    }

    @Override
    public void onFinishPointChanged(Location finishPoint)
    {
        mFinishLocation = finishPoint;
    }

    @Override
    public void onSensorsValuesChanged(Director.SensorsValues sensorsValues)
    {
        mSensorsValues = sensorsValues;
    }

    @Nullable
    public Runnable createComputeRunnable(Context context)
    {
        if (mStartLocation == null || mSensorsValues == null)
        {
            return null;
        }
        return new ComputeRunnable(context, mStartLocation, mFinishLocation, mSensorsValues);
    }
}
