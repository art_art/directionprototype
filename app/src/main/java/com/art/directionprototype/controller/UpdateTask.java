package com.art.directionprototype.controller;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import com.art.directionlibrary.Director;
import com.art.directionlibrary.StandardDirector;
import com.art.directionprototype.controller.computation.ComputeRunnable;
import com.art.directionprototype.controller.computation.InputListener;
import com.art.directionprototype.utils.RepeatingTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Артём on 13.08.2015.
 * Update direction repeating task.
 */
public class UpdateTask extends RepeatingTask implements Director.GetSensorValuesCallback
{
    private ExecutorService mExecutor;
    private InputListener mInputListener;
    private Context mContext;
    private Director mDirector;

    protected UpdateTask(Context context, InputListener inputListener, Handler handler, int interval)
    {
        super(handler, getUpdatePeriodMilliSeconds(interval));
        mContext = context;
        mInputListener = inputListener;
        mExecutor = Executors.newSingleThreadExecutor();
        mDirector = new StandardDirector(context);
        mDirector.onStart();
    }

    @Override
    protected void task()
    {
        mDirector.getSensorValues(this);
    }

    @Override
    public void stopRepeatingTask()
    {
        super.stopRepeatingTask();
        mExecutor.shutdown();
        mDirector.onStop();
    }

    @Override
    public void setInterval(int interval)
    {
        super.setInterval(getUpdatePeriodMilliSeconds(interval));
    }

    private static int getUpdatePeriodMilliSeconds(int updatePeriod)
    {
        return updatePeriod * 100;
    }

    private void notifyFailure()
    {
        ComputeRunnable.broadcastFailure(LocalBroadcastManager.getInstance(mContext));
    }

    @Override
    public void onValuesReady(Director.SensorsValues sensorsValues)
    {
        mInputListener.onSensorsValuesChanged(sensorsValues);
        Runnable computeRunnable = mInputListener.createComputeRunnable(mContext);
        if (computeRunnable == null)
        {
            notifyFailure();
        }
        else
        {
            mExecutor.submit(computeRunnable);
        }
    }

    @Override
    public void onValuesNotReady()
    {
        notifyFailure();
    }

    public static RepeatingTask start(Context context, InputListener inputListener, int interval)
    {
        RepeatingTask task = new UpdateTask(context, inputListener, new Handler(), interval);
        RepeatingTask.start(task);
        return task;
    }
}
