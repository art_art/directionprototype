package com.art.directionprototype.controller.notifier;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;

import com.art.directionprototype.R;
import com.art.directionprototype.controller.computation.ComputeRunnable;

import java.util.ArrayList;

/**
 * Created by art090390 on 06.08.2015.
 * Sound direction notifier.
 */
public class SoundDirectionNotifier extends DirectionNotifier
{
    private static final int SOUND_NOTIFICATION_STANDARD = 0;
    private static final int SOUND_NOTIFICATION_TWO_SOUNDS= 1;
    private static final int SOUND_NOTIFICATION_SIMPLE = 2;

    private static final double SUCCESS_BORDER_MIN = 5d;
    private static final double SUCCESS_BORDER_MAX = 30d;

    private int mFailureSoundId;
    private SoundPool mSoundPool;
    private int mSoundNotificationType;
    private VolumeCalculator mVolumeCalculator;
    private BorderedSoundNotifier mStandardNotifier;
    private BorderedSoundNotifier mTwoSoundsNotifier;
    private SoundNotifier mSimpleNotifier;

    @SuppressWarnings("deprecation")
    public SoundDirectionNotifier(Context context, boolean volumeEditable, int farVolume, int closeVolume, int successBorder)
    {
        super();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            AudioAttributes.Builder audioBuilder = new AudioAttributes.Builder();
            audioBuilder.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION);
            audioBuilder.setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED);
            audioBuilder.setLegacyStreamType(AudioManager.STREAM_NOTIFICATION);
            audioBuilder.setUsage(AudioAttributes.USAGE_NOTIFICATION);

            SoundPool.Builder builder = new SoundPool.Builder();
            builder.setMaxStreams(1);
            builder.setAudioAttributes(audioBuilder.build());
            mSoundPool = builder.build();
        }
        else
        {
            mSoundPool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 0);
        }
        mFailureSoundId = mSoundPool.load(context, R.raw.failure, 1);
        mVolumeCalculator = new VolumeCalculator(volumeEditable, farVolume, closeVolume);
        double successBorderConverted = convertSuccessBorder(successBorder);
        mStandardNotifier = new StandardSoundNotifier(context, mSoundPool, successBorderConverted);
        mTwoSoundsNotifier = new TwoSoundsNotifier(context, mSoundPool, successBorderConverted);
        mSimpleNotifier = new SimpleNotifier(context, mSoundPool);
    }

    private static double convertSuccessBorder(int successBorder)
    {
        return SUCCESS_BORDER_MIN + successBorder * (SUCCESS_BORDER_MAX - SUCCESS_BORDER_MIN) / 100d;
    }

    public void onFarVolumeChanged(int farVolume)
    {
        mVolumeCalculator.onFarVolumeChanged(farVolume);
    }

    public void onCloseVolumeChanged(int closeVolume)
    {
        mVolumeCalculator.onCloseVolumeChanged(closeVolume);
    }

    public void onSuccessBorderChanged(int successBorder)
    {
        double successBorderConverted = convertSuccessBorder(successBorder);
        mStandardNotifier.onSuccessBorderChanged(successBorderConverted);
        mTwoSoundsNotifier.onSuccessBorderChanged(successBorderConverted);
    }

    public void onVolumeEditableChanged(boolean volumeEditable)
    {
        mVolumeCalculator.onVolumeEditableChanged(volumeEditable);
    }

    public void onSoundNotificationTypeChanged(int soundNotificationType)
    {
        mSoundNotificationType = soundNotificationType;
    }

    @Override
    protected void notifyFailure()
    {
        mSoundPool.play(mFailureSoundId, 1f, 1f, 1, 0, 1f);
    }

    private SoundNotifier getSoundNotifier()
    {
        switch (mSoundNotificationType)
        {
        default:
        case SOUND_NOTIFICATION_STANDARD:
            return mStandardNotifier;

        case SOUND_NOTIFICATION_TWO_SOUNDS:
            return mTwoSoundsNotifier;

        case SOUND_NOTIFICATION_SIMPLE:
            return mSimpleNotifier;
        }
    }

    @Override
    protected void notifySuccess(Bundle resultData)
    {
        double angle = resultData.getDouble(ComputeRunnable.EXTRA_ANGLES_DIFFERENCE);
        if (angle < 0 || angle >= 360)
        {
            throw new IllegalArgumentException();
        }

        float distance = resultData.getFloat(ComputeRunnable.EXTRA_LOCATIONS_DISTANCE);
        float volume = mVolumeCalculator.calculate(distance);
        getSoundNotifier().notifyDirection(angle, volume);
    }

    public interface SoundNotifier
    {
        void notifyDirection(double angle, float volume);
    }
}
