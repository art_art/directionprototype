package com.art.directionprototype.controller.notifier;

import android.content.Context;
import android.media.SoundPool;

import com.art.directionprototype.R;

/**
 * Created by Артём on 04.09.2015.
 */
public class TwoSoundsNotifier extends BorderedSoundNotifier
{
    private SoundPool mSoundPool;
    private int mSuccessSoundId;
    private int mSearchSoundId;

    protected TwoSoundsNotifier(Context context, SoundPool soundPool, double successBorder)
    {
        super(successBorder);
        mSoundPool = soundPool;
        mSuccessSoundId = mSoundPool.load(context, R.raw.success, 1);
        mSearchSoundId = mSoundPool.load(context, R.raw.search, 1);
    }

    @Override
    public void notifyDirection(double angle, float volume)
    {
        double successBorder = getSuccessBorder();
        if (angle >= 360d - successBorder || angle <= successBorder)
        {
            mSoundPool.play(mSuccessSoundId, volume, volume, 1, 0, 1f);
        }
        else
        {
            mSoundPool.play(mSearchSoundId, volume, volume, 1, -1, 1f);
        }
    }
}
