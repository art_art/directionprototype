package com.art.directionprototype.controller.computation;

import com.art.directionlibrary.Director;

/**
 * Created by Артём on 12.08.2015.
 */
public interface SensorsValuesListener
{
    void onSensorsValuesChanged(Director.SensorsValues sensorsValues);
}
