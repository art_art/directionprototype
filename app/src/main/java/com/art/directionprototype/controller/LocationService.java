package com.art.directionprototype.controller;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;

import com.art.directionprototype.R;
import com.art.directionprototype.controller.computation.ComputeRunnable;
import com.art.directionprototype.controller.computation.InputListener;
import com.art.directionprototype.controller.notifier.SoundDirectionNotifier;
import com.art.directionprototype.ui.MainActivity;
import com.art.directionprototype.utils.AppSettings;
import com.art.directionprototype.utils.RepeatingTask;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by art090390 on 05.08.2015.
 * Сервис подсчета направления.
 */
public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, AppSettings.SettingsListener
{
    public static final String ACTION_SERVICE_STATE_CHANGED = "ACTION_SERVICE_STATE_CHANGED";
    public static final String EXTRA_SERVICE_STATE = "EXTRA_SERVICE_STATE";
    public static final boolean SERVICE_STATE_STARTED = true;
    public static final boolean SERVICE_STATE_STOPPED = false;
    private static final int NOTIFICATION_ID = 1;
    private static final String PREF_IS_SERVICE_RUNNING = "PREF_IS_SERVICE_RUNNING";
    private static final int LOCATION_DEFAULT_FASTEST_INTERVAL = 200;
    private static final int LOCATION_DEFAULT_INTERVAL = 500;

    private AppSettings mSettings;
    private SoundDirectionNotifier mSoundNotifier;
    private InputListener mInputListener;
    private GoogleApiClient mGoogleApiClient;
    private NotificationCompat.Builder mNotificationBuilder;
    private RepeatingTask mUpdateTask;

    private static SharedPreferences getServicePreferences(Context context)
    {
        return context.getSharedPreferences("LocationService", MODE_PRIVATE);
    }

    private static SharedPreferences getApplicationPreferences(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isServiceRunning(Context context)
    {
        return getServicePreferences(context).getBoolean(PREF_IS_SERVICE_RUNNING, false);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mNotificationBuilder = createNotificationBuilder();

        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this);
        builder.addApi(LocationServices.API);
        builder.addConnectionCallbacks(this);
        builder.addOnConnectionFailedListener(this);
        mGoogleApiClient = builder.build();
        mGoogleApiClient.connect();

        SharedPreferences applicationPreferences = getApplicationPreferences(this);

        mSettings = new AppSettings(this);
        mSettings.register(applicationPreferences);
        mSettings.addListener(this);

        boolean editableVolume = mSettings.getVolumeEditable(applicationPreferences);
        int farVolume = mSettings.getFarVolume(applicationPreferences);
        int closeVolume = mSettings.getCloseVolume(applicationPreferences);
        int successBorder = mSettings.getSuccessBorder(applicationPreferences);
        mSoundNotifier = new SoundDirectionNotifier(this, editableVolume, farVolume, closeVolume, successBorder);
        IntentFilter intentFilter = new IntentFilter(ComputeRunnable.ACTION_COMPUTING_COMPLETED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mSoundNotifier, intentFilter);

        mInputListener = new InputListener(mSettings.getFinishPoint(applicationPreferences));

        mUpdateTask = UpdateTask.start(this, mInputListener, mSettings.getUpdatePeriod(applicationPreferences));

        onStarted();
    }

    @Override
    public void onDestroy()
    {
        onStopped();

        mUpdateTask.stopRepeatingTask();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mSoundNotifier);

        mSettings.removeListener(this);
        mSettings.unregister(getApplicationPreferences(this));

        mGoogleApiClient.disconnect();

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    private NotificationCompat.Builder createNotificationBuilder()
    {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        notificationBuilder.setContentTitle(getString(R.string.app_name));
        notificationBuilder.setContentText(getString(R.string.notification_text));

        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(resultPendingIntent);

        return notificationBuilder;
    }

    private void onStarted()
    {
        getServicePreferences(this).edit().putBoolean(PREF_IS_SERVICE_RUNNING, true).commit();
        Intent stateIntent = new Intent(ACTION_SERVICE_STATE_CHANGED);
        stateIntent.putExtra(EXTRA_SERVICE_STATE, SERVICE_STATE_STARTED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(stateIntent);
        startForeground(NOTIFICATION_ID, mNotificationBuilder.build());
    }

    private void onStopped()
    {
        getServicePreferences(this).edit().putBoolean(PREF_IS_SERVICE_RUNNING, false).commit();
        Intent stateIntent = new Intent(ACTION_SERVICE_STATE_CHANGED);
        stateIntent.putExtra(EXTRA_SERVICE_STATE, SERVICE_STATE_STOPPED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(stateIntent);
        stopForeground(true);
    }

    private LocationRequest createLocationRequest()
    {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(LOCATION_DEFAULT_INTERVAL);
        locationRequest.setFastestInterval(LOCATION_DEFAULT_FASTEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, createLocationRequest(), mInputListener);
    }

    @Override
    public void onConnectionSuspended(int i)
    {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
    }

    @Override
    public void onCloseVolumeChanged(SharedPreferences sharedPreferences, int closeVolume)
    {
        mSoundNotifier.onCloseVolumeChanged(closeVolume);
    }

    @Override
    public void onFarVolumeChanged(SharedPreferences sharedPreferences, int farVolume)
    {
        mSoundNotifier.onFarVolumeChanged(farVolume);
    }

    @Override
    public void onFinishPointChanged(SharedPreferences sharedPreferences, Location finishPoint)
    {
        mInputListener.onFinishPointChanged(finishPoint);
    }

    @Override
    public void onUpdatePeriodChanged(SharedPreferences sharedPreferences, int updatePeriod)
    {
        mUpdateTask.setInterval(updatePeriod);
    }

    @Override
    public void onSuccessBorderChanged(SharedPreferences sharedPreferences, int successBorder)
    {
        mSoundNotifier.onSuccessBorderChanged(successBorder);
    }

    @Override
    public void onVolumeEditableChanged(SharedPreferences sharedPreferences, boolean editable)
    {
        mSoundNotifier.onVolumeEditableChanged(editable);
    }

    @Override
    public void onSoundNotificationTypeChanged(SharedPreferences sharedPreferences, int soundNotificationType)
    {
        mSoundNotifier.onSoundNotificationTypeChanged(soundNotificationType);
    }
}
