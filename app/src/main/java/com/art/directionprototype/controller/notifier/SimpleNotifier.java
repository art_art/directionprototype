package com.art.directionprototype.controller.notifier;

import android.content.Context;
import android.media.SoundPool;

import com.art.directionprototype.R;

/**
 * Created by Артём on 04.09.2015.
 */
public class SimpleNotifier implements SoundDirectionNotifier.SoundNotifier
{
    private SoundPool mSoundPool;
    private int mSuccessSoundId;
    private Double mPreviousAngle;

    public SimpleNotifier(Context context, SoundPool soundPool)
    {
        mSoundPool = soundPool;
        mSuccessSoundId = mSoundPool.load(context, R.raw.success, 1);
    }

    @Override
    public void notifyDirection(double angle, float volume)
    {
        double angleWithOffset = angle >= 180d ? 360d - angle : angle;
        if (mPreviousAngle != null)
        {
            if (mPreviousAngle * angleWithOffset < 0)
            {
                mSoundPool.play(mSuccessSoundId, volume, volume, 1, 0, 1f);
            }
        }
        mPreviousAngle = angleWithOffset;
    }
}
