package com.art.directionprototype.controller.notifier;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.art.directionprototype.controller.computation.ComputeRunnable;

/**
 * Created by Артём on 13.08.2015.
 */
public abstract class DirectionNotifier extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();
        if (action.compareTo(ComputeRunnable.ACTION_COMPUTING_COMPLETED) == 0)
        {
            if (intent.getBooleanExtra(ComputeRunnable.EXTRA_COMPUTING_RESULT, false))
            {
                notifySuccess(intent.getExtras());
            }
            else
            {
                notifyFailure();
            }
        }
    }

    protected abstract void notifyFailure();

    protected abstract void notifySuccess(Bundle resultData);
}
