package com.art.directionprototype.controller.notifier;

/**
 * Created by Артём on 04.09.2015.
 */
public class VolumeCalculator
{
    private static final float DISTANCE_MIN_BORDER = 10f;
    private static final float DISTANCE_MAX_BORDER = 100f;

    private float mFarVolume;
    private float mCloseVolume;
    private boolean mVolumeEditable;

    public VolumeCalculator(boolean volumeEditable, int farVolume, int closeVolume)
    {
        mFarVolume = convertVolume(farVolume);
        mCloseVolume = convertVolume(closeVolume);
        mVolumeEditable = volumeEditable;
    }

    public float calculate(float distance)
    {
        if (mVolumeEditable)
        {
            if (distance <= DISTANCE_MIN_BORDER)
            {
                return mCloseVolume;
            }
            else if (distance > DISTANCE_MAX_BORDER)
            {
                return mFarVolume;
            }
            else
            {
                float distanceDifference = DISTANCE_MIN_BORDER - DISTANCE_MAX_BORDER;
                float k = (mCloseVolume - mFarVolume) / distanceDifference;
                float b = mFarVolume - DISTANCE_MAX_BORDER * (mCloseVolume - mFarVolume) / distanceDifference;

                return k * distance + b;
            }
        }
        else
        {
            return 1.f;
        }
    }

    private static float convertVolume(int volume)
    {
        return volume >= 100 ? 1f : volume / 100f;
    }

    public void onFarVolumeChanged(int farVolume)
    {
        mFarVolume = convertVolume(farVolume);
    }

    public void onCloseVolumeChanged(int closeVolume)
    {
        mCloseVolume = convertVolume(closeVolume);
    }

    public void onVolumeEditableChanged(boolean volumeEditable)
    {
        mVolumeEditable = volumeEditable;
    }
}
