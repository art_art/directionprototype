package com.art.directionprototype.controller.computation;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;

import com.art.directionlibrary.DirectionAngle;
import com.art.directionlibrary.Director;
import com.art.directionlibrary.DirectorException;
import com.art.directionprototype.utils.PointsDirectionAngle;

/**
 * Created by Артём on 13.08.2015.
 * Runnable for computing angles, difference and distance.
 */
public class ComputeRunnable implements Runnable
{
    public static final String ACTION_COMPUTING_COMPLETED = "ACTION_COMPUTING_COMPLETED";
    public static final String EXTRA_COMPUTING_RESULT = "EXTRA_COMPUTING_RESULT";
    public static final String EXTRA_WATCH_ANGLE = "EXTRA_WATCH_ANGLE";
    public static final String EXTRA_MOVE_ANGLE = "EXTRA_MOVE_ANGLE";
    public static final String EXTRA_ANGLES_DIFFERENCE = "EXTRA_ANGLES_DIFFERENCE";
    public static final String EXTRA_START_LOCATION = "EXTRA_START_LOCATION";
    public static final String EXTRA_FINISH_LOCATION = "EXTRA_FINISH_LOCATION";
    public static final String EXTRA_LOCATIONS_DISTANCE = "EXTRA_LOCATIONS_DISTANCE";

    private LocalBroadcastManager mBroadcastManager;
    private Location mStartPoint;
    private Location mFinishPoint;
    private Director.SensorsValues mSensorsValues;

    public ComputeRunnable(Context context, Location startPoint, Location finishPoint, Director.SensorsValues sensorsValues)
    {
        mBroadcastManager = LocalBroadcastManager.getInstance(context);
        mSensorsValues = sensorsValues;
        mStartPoint = startPoint;
        mFinishPoint = finishPoint;
    }

    public static void broadcastFailure(LocalBroadcastManager broadcastManager)
    {
        Intent resultIntent = new Intent(ACTION_COMPUTING_COMPLETED);
        resultIntent.putExtra(EXTRA_COMPUTING_RESULT, false);
        broadcastManager.sendBroadcast(resultIntent);
    }

    @Override
    public void run()
    {

        DirectionAngle watchAngle;
        try
        {
            watchAngle = mSensorsValues.calculate();
        }
        catch (DirectorException e)
        {
            broadcastFailure(mBroadcastManager);
            return;
        }

        PointsDirectionAngle moveAngle = PointsDirectionAngle.fromPoints(mStartPoint, mFinishPoint);

        double moveAngleValue = moveAngle.getAngle();
        if (moveAngleValue < 0 || moveAngleValue >= 360)
        {
            throw new IllegalArgumentException();
        }

        double watchAngleValue = watchAngle.getAngle();
        if (watchAngleValue < 0 || watchAngleValue >= 360)
        {
            throw new IllegalArgumentException();
        }

        double dif = moveAngleValue - watchAngleValue;
        double angle = dif >= 0 ? dif : 360 + dif;
        if (angle < 0 || angle >= 360)
        {
            throw new IllegalStateException();
        }

        Intent resultIntent = new Intent(ACTION_COMPUTING_COMPLETED);
        resultIntent.putExtra(EXTRA_COMPUTING_RESULT, true);
        resultIntent.putExtra(EXTRA_ANGLES_DIFFERENCE, angle);
        resultIntent.putExtra(EXTRA_MOVE_ANGLE, moveAngleValue);
        resultIntent.putExtra(EXTRA_WATCH_ANGLE, watchAngleValue);
        resultIntent.putExtra(EXTRA_START_LOCATION, mStartPoint);
        resultIntent.putExtra(EXTRA_FINISH_LOCATION, mFinishPoint);
        resultIntent.putExtra(EXTRA_LOCATIONS_DISTANCE, moveAngle.getDistance());
        mBroadcastManager.sendBroadcast(resultIntent);
    }
}
