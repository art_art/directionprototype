package com.art.directionprototype.controller.notifier;

import android.content.Context;
import android.media.SoundPool;

import com.art.directionprototype.R;

/**
 * Created by Артём on 04.09.2015.
 */
public class StandardSoundNotifier extends BorderedSoundNotifier
{
    private static final double DEGREES_DISMISS_BORDER_LEFT = 120d;
    private static final double DEGREES_DISMISS_BORDER_RIGHT = 240d;

    private SoundPool mSoundPool;
    private int mSuccessSoundId;
    private int mMiddleSoundId;

    public StandardSoundNotifier(Context context, SoundPool soundPool, double successBorder)
    {
        super(successBorder);

        mSoundPool = soundPool;
        mSuccessSoundId = mSoundPool.load(context, R.raw.success, 1);
        mMiddleSoundId = mSoundPool.load(context, R.raw.middle, 1);
    }

    @Override
    public void notifyDirection(double angle, float volume)
    {
        double successBorder = getSuccessBorder();
        int soundId = mSuccessSoundId;
        float leftVolume = 0f, rightVolume = 0f;
        if (angle >= 360d - successBorder || angle <= successBorder)
        {
            leftVolume = volume;
            rightVolume = volume;
        }
        else
        {
            soundId = mMiddleSoundId;
            if (angle < DEGREES_DISMISS_BORDER_LEFT)
            {
                rightVolume = volume;
            }
            else if (angle > DEGREES_DISMISS_BORDER_RIGHT)
            {
                leftVolume = volume;
            }
        }

        mSoundPool.play(soundId, leftVolume, rightVolume, 1, 0, 1f);
    }
}
