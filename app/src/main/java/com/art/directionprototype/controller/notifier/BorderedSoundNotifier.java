package com.art.directionprototype.controller.notifier;

/**
 * Created by Артём on 04.09.2015.
 */
public abstract class BorderedSoundNotifier implements SoundDirectionNotifier.SoundNotifier
{
    private double mSuccessBorder;

    protected BorderedSoundNotifier(double successBorder)
    {
        mSuccessBorder = successBorder;
    }

    public final void onSuccessBorderChanged(double successBorder)
    {
        mSuccessBorder = successBorder;
    }

    protected double getSuccessBorder()
    {
        return mSuccessBorder;
    }
}
