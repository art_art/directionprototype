package com.art.directionprototype.controller.computation;

import android.location.Location;

/**
 * Created by Артём on 12.08.2015.
 */
public interface FinishPointListener
{
    void onFinishPointChanged(Location finishPoint);
}
