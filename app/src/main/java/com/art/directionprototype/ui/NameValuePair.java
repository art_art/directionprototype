package com.art.directionprototype.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.art.directionprototype.R;

/**
 * Created by Артём on 18.08.2015.
 */
public class NameValuePair extends LinearLayout
{
    private TextView mName;
    private TextView mValue;

    public NameValuePair(Context context)
    {
        this(context, null);
    }

    public NameValuePair(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public NameValuePair(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NameValuePair(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void initView(Context context)
    {
        View rootView = inflate(context, R.layout.name_value_pair, this);
        mName = (TextView)rootView.findViewById(R.id.name);
        mValue = (TextView)rootView.findViewById(R.id.value);
    }

    private void initProperties(Context context, AttributeSet attrs)
    {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.com_art_directionprototype_ui_NameValuePair);

        String name = array.getString(R.styleable.com_art_directionprototype_ui_NameValuePair_name);
        if (name != null)
        {
            mName.setText(name);
        }

        String value = array.getString(R.styleable.com_art_directionprototype_ui_NameValuePair_value);
        setValue(value);

        array.recycle();
    }

    private void init(Context context, AttributeSet attrs)
    {
        initView(context);

        initProperties(context, attrs);
    }

    public void setValue(String value)
    {
        if (value != null)
        {
            mValue.setText(value);
        }
    }

    public void setValue(double value)
    {
        setValue(Double.toString(value));
    }

    public void setValue(float value)
    {
        setValue(Float.toString(value));
    }
}
