package com.art.directionprototype.ui;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.art.directionprototype.R;
import com.art.directionprototype.utils.AppSettings;

/**
 * Created by art090390 on 06.08.2015.
 * Application settings fragment.
 */
public class SettingsFragment extends PreferenceFragment implements AppSettings.SettingsListener
{
    private SeekBarPreference mFarVolumePref;
    private SeekBarPreference mCloseVolumePref;
    private AppSettings mSettings;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, false);

        mSettings = new AppSettings(getActivity());
        mFarVolumePref = (SeekBarPreference)findPreference(mSettings.getFarVolumeKey());
        mCloseVolumePref = (SeekBarPreference)findPreference(mSettings.getCloseVolumeKey());
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mSettings.addListener(this);
        mSettings.register(getPreferenceManager().getSharedPreferences());
    }

    @Override
    public void onPause()
    {
        mSettings.unregister(getPreferenceManager().getSharedPreferences());
        mSettings.removeListener(this);
        super.onPause();
    }

    @Override
    public void onCloseVolumeChanged(SharedPreferences sharedPreferences, int closeVolume)
    {
        int farVolume = mSettings.getFarVolume(sharedPreferences);

        if (farVolume > closeVolume)
        {
            mFarVolumePref.setValue(closeVolume);
        }
    }

    @Override
    public void onFarVolumeChanged(SharedPreferences sharedPreferences, int farVolume)
    {
        int closeVolume = mSettings.getCloseVolume(sharedPreferences);

        if (farVolume > closeVolume)
        {
            mCloseVolumePref.setValue(farVolume);
        }
    }

    @Override
    public void onFinishPointChanged(SharedPreferences sharedPreferences, Location finishPoint)
    {
    }

    @Override
    public void onUpdatePeriodChanged(SharedPreferences sharedPreferences, int updatePeriod)
    {
    }

    @Override
    public void onSuccessBorderChanged(SharedPreferences sharedPreferences, int successBorder)
    {
    }

    @Override
    public void onVolumeEditableChanged(SharedPreferences sharedPreferences, boolean editable)
    {
    }

    @Override
    public void onSoundNotificationTypeChanged(SharedPreferences sharedPreferences, int soundNotificationType)
    {
    }
}
