package com.art.directionprototype.ui;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.Preference;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.art.directionprototype.R;
import com.art.locationpicker.Contract;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by art090390 on 10.08.2015.
 * Класс настройки местоположения.
 */
public class LocationPreference extends Preference implements View.OnClickListener
{
    private static final String JSON_LATITUDE = "latitude";
    private static final String JSON_LONGITUDE = "longitude";
    private Location mLocation;
    private TextView mLatitudeView;
    private TextView mLongitudeView;
    private String mTitle;
    private BroadcastReceiver mLocationPickedReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getBooleanExtra(Contract.RESULT_STATUS, false))
            {
                mLocation = intent.getParcelableExtra(Contract.RESULT_LOCATION);
                persistString(locationtoString(mLocation));
                display();
            }
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(this);
        }
    };

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressWarnings("unused")
    public LocationPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    public LocationPreference(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public LocationPreference(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    @SuppressWarnings("unused")
    public LocationPreference(Context context)
    {
        this(context, null);
    }

    public static Location parseLocation(String location)
    {
        double latitude;
        double longitude;
        try
        {
            JSONObject locationObject = new JSONObject(location);
            latitude = locationObject.getDouble(JSON_LATITUDE);
            longitude = locationObject.getDouble(JSON_LONGITUDE);
        }
        catch (JSONException e)
        {
            throw new IllegalArgumentException(e);
        }

        Location parsedLocation = new Location("com.art.directionprototype");
        parsedLocation.setLatitude(latitude);
        parsedLocation.setLongitude(longitude);
        return parsedLocation;
    }

    public static String locationtoString(Location location)
    {
        JSONObject locationObject = new JSONObject();
        try
        {
            locationObject.put(JSON_LONGITUDE, location.getLongitude());
            locationObject.put(JSON_LATITUDE, location.getLatitude());
        }
        catch (JSONException e)
        {
            throw new IllegalStateException(e);
        }
        return locationObject.toString();
    }

    private void init(Context context, AttributeSet attrs)
    {
        int[] standardAttrs = new int[] {android.R.attr.title};
        TypedArray a = context.obtainStyledAttributes(attrs, standardAttrs);
        String title = a.getString(0);
        mTitle = title != null ? title : context.getString(R.string.location_pref_default_title);
        a.recycle();

        setLayoutResource(R.layout.pref_location);
    }

    protected void display()
    {
        mLatitudeView.setText(Double.toString(mLocation.getLatitude()));
        mLongitudeView.setText(Double.toString(mLocation.getLongitude()));
    }

    @Override
    protected void onBindView(@NonNull View rootView)
    {
        super.onBindView(rootView);

        mLatitudeView = (TextView)rootView.findViewById(R.id.text_latitude);
        mLongitudeView = (TextView)rootView.findViewById(R.id.text_longitude);
        display();

        ImageButton pickLocationButton = (ImageButton)rootView.findViewById(R.id.pick_location);
        pickLocationButton.setOnClickListener(this);

        TextView title = (TextView)rootView.findViewById(R.id.title);
        title.setText(mTitle);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue)
    {
        if (restorePersistedValue)
        {
            String defaultLocation = getContext().getResources().getString(R.string.location_default);
            String persisted = getPersistedString(defaultLocation);
            mLocation = parseLocation(persisted);
        }
        else
        {
            mLocation = (Location)defaultValue;
            persistString(locationtoString(mLocation));
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index)
    {
        String defaultValue = a.getString(index);
        if (defaultValue == null)
        {
            defaultValue = getContext().getResources().getString(R.string.location_default);
        }
        return parseLocation(defaultValue);
    }

    @Override
    public void onClick(View v)
    {
        getContext().registerReceiver(mLocationPickedReceiver, Contract.sLocationPickedIntentFilter);
        Intent pickLocationIntent = new Intent(Contract.ACTION_PICK_LOCATION);
        pickLocationIntent.putExtra(Contract.EXTRA_ENABLE_MY, true);
        if (mLocation != null)
        {
            pickLocationIntent.putExtra(Contract.EXTRA_LOCATION, mLocation);
        }
        getContext().startActivity(pickLocationIntent);
    }

    @Override
    protected Parcelable onSaveInstanceState()
    {
        final Parcelable superState = super.onSaveInstanceState();
        if (isPersistent())
        {
            return superState;
        }

        final SavedState myState = new SavedState(superState);
        myState.mLocation = locationtoString(mLocation);
        return myState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state)
    {
        if (state == null || !state.getClass().equals(SavedState.class))
        {
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState myState = (SavedState)state;
        super.onRestoreInstanceState(myState.getSuperState());

        mLocation = parseLocation(myState.mLocation);
    }

    private static class SavedState extends BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>()
                {
                    public SavedState createFromParcel(Parcel in)
                    {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size)
                    {
                        return new SavedState[size];
                    }
                };
        private String mLocation;

        public SavedState(Parcelable superState)
        {
            super(superState);
        }

        public SavedState(Parcel source)
        {
            super(source);
            mLocation = source.readString();
        }

        @Override
        public void writeToParcel(@NonNull Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeString(mLocation);
        }
    }
}
