package com.art.directionprototype.ui;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.art.directionprototype.R;
import com.art.directionprototype.controller.LocationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity
{
    private static final int REQUEST_RESOLVE_ERROR = 1;

    private static final IntentFilter sServiceStateFilter = new IntentFilter(LocationService.ACTION_SERVICE_STATE_CHANGED);

    private Intent mServiceIntent;
    private MenuItem mServiceMenuItem;
    private LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mServiceActionsReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (action.compareTo(LocationService.ACTION_SERVICE_STATE_CHANGED) == 0)
            {
                mServiceMenuItem.setEnabled(true);
                boolean isRunning = intent.getBooleanExtra(LocationService.EXTRA_SERVICE_STATE, LocationService.SERVICE_STATE_STOPPED);
                mServiceMenuItem.setIcon(isRunning ? R.mipmap.ic_compass_disabled : R.mipmap.ic_compass);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        mServiceIntent = new Intent(this, LocationService.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mServiceMenuItem = menu.findItem(R.id.action_service);

        boolean isServiceRunning = LocationService.isServiceRunning(this);
        mServiceMenuItem.setIcon(isServiceRunning ? R.mipmap.ic_compass_disabled : R.mipmap.ic_compass);

        return true;
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mLocalBroadcastManager.registerReceiver(mServiceActionsReceiver, sServiceStateFilter);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int availability = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS != availability)
        {
            Dialog errorDialog = googleApiAvailability.getErrorDialog(this, availability, REQUEST_RESOLVE_ERROR);
            errorDialog.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_service)
        {
            if (LocationService.isServiceRunning(this))
            {
                stopService(mServiceIntent);
            }
            else
            {
                startService(mServiceIntent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        mLocalBroadcastManager.unregisterReceiver(mServiceActionsReceiver);
    }
}
