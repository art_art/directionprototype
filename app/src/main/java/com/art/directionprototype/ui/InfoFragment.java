package com.art.directionprototype.ui;


import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.art.directionprototype.R;
import com.art.directionprototype.controller.computation.ComputeRunnable;
import com.art.directionprototype.controller.notifier.DirectionNotifier;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoFragment extends Fragment
{
    private LocalBroadcastManager mBroadcastManager;
    private IntentFilter sInfoIntentFilter = new IntentFilter(ComputeRunnable.ACTION_COMPUTING_COMPLETED);
    private NameValuePair mStartPointLatitude;
    private NameValuePair mStartPointLongitude;
    private NameValuePair mFinishPointLatitude;
    private NameValuePair mFinishPointLongitude;
    private NameValuePair mDistance;
    private NameValuePair mWatchAngle;
    private NameValuePair mMoveAngle;
    private NameValuePair mAnglesDifference;
    private ImageView mLastResult;
    private TextView mLastUpdateTime;
    private BroadcastReceiver mInfoReceiver = new DirectionNotifier()
    {
        @Override
        protected void notifyFailure()
        {
            updateTime();
            mLastResult.setImageResource(R.mipmap.ic_action_cancel);
        }

        @Override
        protected void notifySuccess(Bundle resultData)
        {
            updateTime();
            mLastResult.setImageResource(R.mipmap.ic_action_accept);
            Location startPoint = resultData.getParcelable(ComputeRunnable.EXTRA_START_LOCATION);
            mStartPointLatitude.setValue(startPoint.getLatitude());
            mStartPointLongitude.setValue(startPoint.getLongitude());
            Location finishPoint = resultData.getParcelable(ComputeRunnable.EXTRA_FINISH_LOCATION);
            mFinishPointLatitude.setValue(finishPoint.getLatitude());
            mFinishPointLongitude.setValue(finishPoint.getLongitude());
            mDistance.setValue(resultData.getFloat(ComputeRunnable.EXTRA_LOCATIONS_DISTANCE));
            mWatchAngle.setValue(resultData.getDouble(ComputeRunnable.EXTRA_WATCH_ANGLE));
            mMoveAngle.setValue(resultData.getDouble(ComputeRunnable.EXTRA_MOVE_ANGLE));
            mAnglesDifference.setValue(resultData.getDouble(ComputeRunnable.EXTRA_ANGLES_DIFFERENCE));
        }
    };

    public InfoFragment()
    {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_info, container, false);
        mStartPointLatitude = (NameValuePair)rootView.findViewById(R.id.start_point_latitude);
        mStartPointLongitude = (NameValuePair)rootView.findViewById(R.id.start_point_longitude);
        mFinishPointLatitude = (NameValuePair)rootView.findViewById(R.id.finish_point_latitude);
        mFinishPointLongitude = (NameValuePair)rootView.findViewById(R.id.finish_point_longitude);
        mDistance = (NameValuePair)rootView.findViewById(R.id.distance);
        mWatchAngle = (NameValuePair)rootView.findViewById(R.id.watch_angle);
        mMoveAngle = (NameValuePair)rootView.findViewById(R.id.move_angle);
        mAnglesDifference = (NameValuePair)rootView.findViewById(R.id.difference);
        mLastResult = (ImageView)rootView.findViewById(R.id.last_result);
        mLastUpdateTime = (TextView)rootView.findViewById(R.id.last_update_time);
        updateTime();
        return rootView;
    }

    private void updateTime()
    {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        mLastUpdateTime.setText(sdf.format(now));
    }

    @Override
    public void onStart()
    {
        super.onStart();
        mBroadcastManager.registerReceiver(mInfoReceiver, sInfoIntentFilter);
    }

    @Override
    public void onStop()
    {
        mBroadcastManager.unregisterReceiver(mInfoReceiver);
        super.onStop();
    }
}
