package com.art.directionprototype.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.Preference;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.art.directionprototype.R;

public class SeekBarPreference extends Preference implements OnSeekBarChangeListener
{
    private static final int DEFAULT_MAX_VALUE = 100;
    private static final int DEFAULT_VALUE = 0;

    private int mValue;
    private int mMax;
    private boolean mTrackingTouch;
    private String mTitle;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressWarnings("unused")
    public SeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    public SeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public SeekBarPreference(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    @SuppressWarnings("unused")
    public SeekBarPreference(Context context)
    {
        this(context, null);
    }

    private void init(Context context, AttributeSet attrs)
    {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.com_art_directionprototype_SeekBarPreference, 0, 0);
        mMax = a.getInt(R.styleable.com_art_directionprototype_SeekBarPreference_maxValue, DEFAULT_MAX_VALUE);
        a.recycle();

        int[] standardAttrs = new int[] {android.R.attr.title};
        a = context.obtainStyledAttributes(attrs, standardAttrs);
        String title = a.getString(0);
        mTitle = title != null ? title : context.getString(R.string.seek_bar_pref_default_title);
        a.recycle();

        setLayoutResource(R.layout.pref_seek_bar);
    }

    @Override
    protected void onBindView(@NonNull View view)
    {
        super.onBindView(view);

        SeekBar seekBar = (SeekBar)view.findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setMax(mMax);
        seekBar.setProgress(mValue);
        seekBar.setEnabled(isEnabled());

        TextView title = (TextView)view.findViewById(R.id.title);
        title.setText(mTitle);
    }

    @Override
    public CharSequence getSummary()
    {
        return null;
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue)
    {
        setValue(restoreValue ? getPersistedInt(mValue) : (Integer)defaultValue);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index)
    {
        return a.getInt(index, DEFAULT_VALUE);
    }

    @SuppressWarnings("unused")
    public void setMax(int max)
    {
        if (max != mMax)
        {
            mMax = max;
            notifyChanged();
        }
    }

    public void setValue(int value)
    {
        setValue(value, true);
    }

    private void setValue(int value, boolean notifyChanged)
    {
        if (value > mMax)
        {
            value = mMax;
        }
        if (value < 0)
        {
            value = 0;
        }
        if (value != mValue)
        {
            mValue = value;
            persistInt(value);
            if (notifyChanged)
            {
                notifyChanged();
            }
        }
    }

    @SuppressWarnings("unused")
    public int getValue()
    {
        return mValue;
    }

    private void syncValue(SeekBar seekBar)
    {
        int value = seekBar.getProgress();
        if (value != mValue)
        {
            if (callChangeListener(value))
            {
                setValue(value, false);
            }
            else
            {
                seekBar.setProgress(mValue);
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
    {
        if (fromUser && !mTrackingTouch)
        {
            syncValue(seekBar);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {
        mTrackingTouch = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {
        mTrackingTouch = false;
        if (seekBar.getProgress() != mValue)
        {
            syncValue(seekBar);
        }
    }

    @Override
    protected Parcelable onSaveInstanceState()
    {
        final Parcelable superState = super.onSaveInstanceState();
        if (isPersistent())
        {
            return superState;
        }

        final SavedState myState = new SavedState(superState);
        myState.value = mValue;
        myState.max = mMax;
        return myState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state)
    {
        if (!state.getClass().equals(SavedState.class))
        {
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState myState = (SavedState)state;
        super.onRestoreInstanceState(myState.getSuperState());
        mValue = myState.value;
        mMax = myState.max;
        notifyChanged();
    }

    private static class SavedState extends BaseSavedState
    {
        int value;
        int max;

        public SavedState(Parcel source)
        {
            super(source);
            value = source.readInt();
            max = source.readInt();
        }

        @Override
        public void writeToParcel(@NonNull Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeInt(value);
            dest.writeInt(max);
        }

        public SavedState(Parcelable superState)
        {
            super(superState);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>()
                {
                    public SavedState createFromParcel(Parcel in)
                    {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size)
                    {
                        return new SavedState[size];
                    }
                };
    }
}
