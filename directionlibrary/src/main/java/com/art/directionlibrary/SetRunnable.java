package com.art.directionlibrary;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Артём on 23.07.2015.
 * Runnable setting values.
 */
public abstract class SetRunnable extends SimpleCallbackRunnable
{
    private float[] mSource;

    protected SetRunnable(@NonNull Handler callbackHandler, @Nullable Director.SimpleCallback callback, @NonNull float[] source, int length)
    {
        super(callbackHandler, callback);
        if (source.length != length)
            throw new IllegalArgumentException("Нужно передать на вход массивы размера " + length);
        mSource = new float[length];
        System.arraycopy(source, 0, mSource, 0, length);
    }

    protected abstract void set(@NonNull float[] source);

    @Override
    protected final void runOperation()
    {
        set(mSource);
    }
}
