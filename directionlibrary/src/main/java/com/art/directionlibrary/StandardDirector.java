package com.art.directionlibrary;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;

/**
 * Created by РђСЂС‚С‘Рј on 22.07.2015.
 * Director using standard device sensors.
 */
public class StandardDirector extends Director implements SensorEventListener
{
    private SensorManager mSensorManager;
    private Sensor mAccelerometerSensor;
    private Sensor mMagneticFieldSensor;

    private int mSamplingPeriod = SensorManager.SENSOR_DELAY_NORMAL;

    public StandardDirector(@NonNull Context context)
    {
        super(null);
        mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        mMagneticFieldSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mAccelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        mSensorManager.registerListener(this, mAccelerometerSensor, mSamplingPeriod);
        mSensorManager.registerListener(this, mMagneticFieldSensor, mSamplingPeriod);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected SensorsValues getSensorsValues(@NonNull float[] acceleration, @NonNull float[] magneticField)
    {
        return new SensorsValues(acceleration, magneticField)
        {
            @Override
            public DirectionAngle calculate() throws DirectorException
            {
                float[] R = new float[9];
                if (SensorManager.getRotationMatrix(R, null, getAcceleration(), getMagneticField()))
                {
                    float[] angles = SensorManager.getOrientation(R, new float[3]);
                    double angle = Math.toDegrees(angles[0] % Math.PI);
                    return new DirectionAngle(angle >= 0 ? angle : 360 + angle);
                }
                throw new DirectorException("Ошибка SensorManager.getRotationMatrix");
            }
        };
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        switch (event.sensor.getType())
        {
        case Sensor.TYPE_ACCELEROMETER:
            setAcceleration(event.values, null);
            break;

        case Sensor.TYPE_MAGNETIC_FIELD:
            setMagneticField(event.values, null);
            break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    }

    public void setSamplingPeriod(int samplingPeriod)
    {
        mSamplingPeriod = samplingPeriod;
    }
}
