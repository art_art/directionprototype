package com.art.directionlibrary;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.art.directionlibrary.CallbackExecutor;
import com.art.directionlibrary.Director;

/**
 * Created by art090390 on 23.07.2015.
 * Runnable with simple callback.
 */
public abstract class SimpleCallbackRunnable implements Runnable
{
    private CallbackExecutor mCallbackExecutor;
    private Director.SimpleCallback mCallback;

    protected SimpleCallbackRunnable(@NonNull Handler callbackHandler, @Nullable Director.SimpleCallback callback)
    {
        mCallbackExecutor = new CallbackExecutor(callbackHandler);
        mCallback = callback;
    }

    protected abstract void runOperation();

    @Override
    public final void run()
    {
        runOperation();
        if (mCallback != null)
        {
            mCallbackExecutor.execute(new CallbackExecutor.CallbackWrapper()
            {
                @Override
                public void callback()
                {
                    mCallback.onFinished();
                }
            });
        }
    }
}
