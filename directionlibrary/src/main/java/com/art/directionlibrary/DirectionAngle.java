package com.art.directionlibrary;

/**
 * Created by Артём on 12.08.2015.
 */
public class DirectionAngle
{
    private double mAngle;

    public DirectionAngle(double angle)
    {
        mAngle = angle;
    }

    public double getAngle()
    {
        return mAngle;
    }
}
