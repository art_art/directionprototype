package com.art.directionlibrary;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Артём on 22.07.2015.
 * Class for managing sensors values.
 */
public abstract class Director
{
    private float[] mAcceleration;
    private float[] mMagneticField;
    private Handler mSensorsAccessHandler;

    protected Director(@Nullable Handler sensorsAccessHandler)
    {
        mSensorsAccessHandler = sensorsAccessHandler == null ? new Handler() : sensorsAccessHandler;
    }

    public void getSensorValues(@NonNull GetSensorValuesCallback callback)
    {
        Handler callbackHandler = new Handler();
        mSensorsAccessHandler.post(new GetSensorValuesRunnable(callbackHandler, callback));
    }

    protected void setMagneticField(@NonNull float[] magneticField, @Nullable SimpleCallback callback)
    {
        Handler callbackHandler = new Handler();
        mSensorsAccessHandler.post(new SetMagneticFieldRunnable(callbackHandler, callback, magneticField));
    }

    protected void setAcceleration(@NonNull float[] acceleration, @Nullable SimpleCallback callback)
    {
        Handler callbackHandler = new Handler();
        mSensorsAccessHandler.post(new SetAccelerationRunnable(callbackHandler, callback, acceleration));
    }

    private void resetSensorValues(@Nullable SimpleCallback callback)
    {
        Handler callbackHandler = new Handler();
        mSensorsAccessHandler.post(new ResetSensorValuesRunnable(callbackHandler, callback));
    }

    public void onStart()
    {
    }

    public void onStop()
    {
        resetSensorValues(null);
    }

    protected abstract SensorsValues getSensorsValues(@NonNull float[] acceleration, @NonNull float[] magneticField);

    public abstract static class SensorsValues
    {
        private float[] mAcceleration;
        private float[] mMagneticField;

        public SensorsValues(float[] acceleration, float[] magneticField)
        {
            mAcceleration = acceleration;
            mMagneticField = magneticField;
        }

        protected float[] getAcceleration()
        {
            return mAcceleration;
        }

        protected float[] getMagneticField()
        {
            return mMagneticField;
        }

        public abstract DirectionAngle calculate() throws DirectorException;
    }

    public interface SimpleCallback
    {
        void onFinished();
    }

    public interface GetSensorValuesCallback
    {
        void onValuesReady(SensorsValues sensorsValues);

        void onValuesNotReady();
    }

    private class GetSensorValuesRunnable implements Runnable
    {
        private CallbackExecutor mCallbackExecutor;
        private GetSensorValuesCallback mCallback;

        protected GetSensorValuesRunnable(@NonNull Handler callbackHandler, final @NonNull GetSensorValuesCallback callback)
        {
            mCallbackExecutor = new CallbackExecutor(callbackHandler);
            mCallback = callback;
        }

        @Override
        public void run()
        {
            if (mAcceleration == null || mMagneticField == null)
            {
                mCallbackExecutor.execute(new CallbackExecutor.CallbackWrapper()
                {
                    @Override
                    public void callback()
                    {
                        mCallback.onValuesNotReady();
                    }
                });
            }
            else
            {
                final float[] acceleration = new float[3];
                final float[] magneticField = new float[3];

                System.arraycopy(mAcceleration, 0, acceleration, 0, 3);
                System.arraycopy(mMagneticField, 0, magneticField, 0, 3);

                mCallbackExecutor.execute(new CallbackExecutor.CallbackWrapper()
                {
                    @Override
                    public void callback()
                    {
                        mCallback.onValuesReady(getSensorsValues(acceleration, magneticField));
                    }
                });
            }
        }
    }

    private class SetAccelerationRunnable extends SetRunnable
    {
        protected SetAccelerationRunnable(@NonNull Handler callbackHandler, @Nullable SimpleCallback callback, @NonNull float[] acceleration)
        {
            super(callbackHandler, callback, acceleration, 3);
        }

        @Override
        protected void set(@NonNull float[] source)
        {
            mAcceleration = source;
        }
    }

    private class SetMagneticFieldRunnable extends SetRunnable
    {
        protected SetMagneticFieldRunnable(@NonNull Handler callbackHandler, @Nullable SimpleCallback callback, @NonNull float[] magneticField)
        {
            super(callbackHandler, callback, magneticField, 3);
        }

        @Override
        protected void set(@NonNull float[] source)
        {
            mMagneticField = source;
        }
    }

    private class ResetSensorValuesRunnable extends SimpleCallbackRunnable
    {
        protected ResetSensorValuesRunnable(@NonNull Handler callbackHandler, @Nullable SimpleCallback callback)
        {
            super(callbackHandler, callback);
        }

        @Override
        public void runOperation()
        {
            mAcceleration = null;
            mMagneticField = null;
        }
    }
}
