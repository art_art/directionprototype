package com.art.directionlibrary;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by art090390 on 05.08.2015.
 * Class for executing callbacks on the exact handler.
 */
public class CallbackExecutor
{
    private Handler mCallbackHandler;

    public CallbackExecutor(@NonNull Handler callbackHandler)
    {
        mCallbackHandler = callbackHandler;
    }

    public void execute(@Nullable final CallbackWrapper callbackWrapper)
    {
        if (callbackWrapper != null)
        {
            mCallbackHandler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    callbackWrapper.callback();
                }
            });
        }
    }

    public interface CallbackWrapper
    {
        void callback();
    }
}
