package com.art.directionlibrary;

/**
 * Created by art090390 on 05.08.2015.
 * Library exception.
 */
public class DirectorException extends Exception
{
    public DirectorException(String message)
    {
        super(message);
    }
}
