package com.art.locationpicker;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMapLongClickListener
{
    private EditText mLatitudeView;
    private EditText mLongitudeView;
    private MapFragment mMapFragment;
    private LatLng mLocation;
    private Marker mMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        if (savedInstanceState == null)
        {
            Location location = getIntent().getExtras().getParcelable(Contract.EXTRA_LOCATION);
            if (location != null)
            {
                mLocation = new LatLng(location.getLatitude(), location.getLongitude());
            }
        }
        else
        {
            mLocation = savedInstanceState.getParcelable(Contract.EXTRA_LOCATION);
        }

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(this);

        mMapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        mLatitudeView = (EditText)findViewById(R.id.edit_latitude);
        mLatitudeView.setOnEditorActionListener(new EditTextDoneListener()
        {
            @Override
            protected void onDone(String text)
            {
                try
                {
                    double latitude = Location.convert(text);
                    double longitude = mLocation.longitude;
                    mLocation = new LatLng(latitude, longitude);
                    mMapFragment.getMapAsync(MapActivity.this);
                }
                catch (IllegalArgumentException ex)
                {
                    Toast.makeText(MapActivity.this, R.string.toast_wrong_latitude, Toast.LENGTH_SHORT).show();
                }
            }
        });

        mLongitudeView = (EditText)findViewById(R.id.edit_longitude);
        mLongitudeView.setOnEditorActionListener(new EditTextDoneListener()
        {
            @Override
            protected void onDone(String text)
            {
                try
                {
                    double longitude = Location.convert(text);
                    double latitude = mLocation.latitude;
                    mLocation = new LatLng(latitude, longitude);
                    mMapFragment.getMapAsync(MapActivity.this);
                }
                catch (IllegalArgumentException ex)
                {
                    Toast.makeText(MapActivity.this, R.string.toast_wrong_longitude, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        if (mLocation == null)
        {
            Toast.makeText(this, R.string.toast_pick_location, Toast.LENGTH_SHORT).show();
            return;
        }

        Location resultLocation = new Location("com.art.locationpicker");
        resultLocation.setLatitude(mLocation.latitude);
        resultLocation.setLongitude(mLocation.longitude);

        Intent result = new Intent(Contract.ACTION_LOCATION_PICKER_DONE);
        result.putExtra(Contract.RESULT_STATUS, true);
        result.putExtra(Contract.RESULT_LOCATION, resultLocation);
        sendBroadcast(result);

        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        if (mLocation != null)
        {
            outState.putParcelable(Contract.EXTRA_LOCATION, mLocation);
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent result = new Intent(Contract.ACTION_LOCATION_PICKER_DONE);
        result.putExtra(Contract.RESULT_STATUS, false);
        sendBroadcast(result);
    }

    private void prepareMap(GoogleMap map)
    {
        Intent intent = getIntent();
        if (intent != null)
        {
            Bundle arguments = intent.getExtras();
            if (arguments != null)
            {
                map.setMyLocationEnabled(arguments.getBoolean(Contract.EXTRA_ENABLE_MY, false));
            }
        }

        map.setOnMapLongClickListener(this);
    }

    private void displayCoords()
    {
        mLatitudeView.setText(Double.toString(mLocation.latitude));
        mLongitudeView.setText(Double.toString(mLocation.longitude));
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        prepareMap(googleMap);

        if (mLocation != null)
        {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, 13));

            if (mMarker != null)
            {
                mMarker.remove();
                mMarker = null;
            }

            mMarker = googleMap.addMarker(new MarkerOptions().position(mLocation));

            displayCoords();
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng)
    {
        mLocation = latLng;
        mMapFragment.getMapAsync(this);
    }

    private static abstract class EditTextDoneListener implements TextView.OnEditorActionListener
    {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
        {
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                onDone(v.getText().toString());
                return true;
            }
            return false;
        }

        protected abstract void onDone(String text);
    }
}
