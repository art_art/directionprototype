package com.art.locationpicker;

import android.content.IntentFilter;

/**
 * Created by art090390 on 10.08.2015.
 * Класс контракт для использования модуля.
 */
public class Contract
{
    public static final String ACTION_PICK_LOCATION = "com.art.locationpicker.Contract.ACTION_PICK_LOCATION";
    public static final String EXTRA_LOCATION = "EXTRA_LOCATION";
    public static final String EXTRA_ENABLE_MY = "EXTRA_ENABLE_MY";
    public static final String ACTION_LOCATION_PICKER_DONE = "com.art.locationpicker.Contract.ACTION_LOCATION_PICKER_DONE";
    public static final IntentFilter sLocationPickedIntentFilter = new IntentFilter(ACTION_LOCATION_PICKER_DONE);
    public static final String RESULT_STATUS = "RESULT_STATUS";
    public static final String RESULT_LOCATION = "RESULT_LOCATION";
}
